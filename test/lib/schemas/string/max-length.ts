import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('createMaxLengthSchema', ({test}) => {
    test('maxLength positive', ({same, end}) => {
        const schema = createSchema({
            type: "string",
            maxLength: 1
        });

        same(schema.description, 'of type \'string\' and shorter than or equal to 1');
        same(schema.validate('1').length, 0);
        same(schema.validate('11').length, 1);
        same(schema.validate('11')[0].message, 'must be shorter than or equal to 1');
        same(schema.validate('11')[0].path, undefined);
        end();
    });
});
