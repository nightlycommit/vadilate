import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('format', ({test}) => {
    test('date', ({same, end}) => {
        const schema = createSchema({
            type: "string",
            format: 'date'
        });

        const expectedDescription = 'matches RFC3339 full-date profile';

        same(schema.description, `of type 'string' and ${expectedDescription}`);
        same(schema.validate('2016-04-06').length, 0);
        same(schema.validate('2016-04').length, 1);
        same(schema.validate('2016-04')[0].message, `must match RFC3339 full-date profile`);
        same(schema.validate('2016-04')[0].path, undefined);
        same(schema.validate('2016').length, 1);
        same(schema.validate('2016')[0].message, `must match RFC3339 full-date profile`);
        same(schema.validate('2016')[0].path, undefined);
        same(schema.validate('a').length, 1);
        same(schema.validate('a')[0].message, `must match RFC3339 full-date profile`);
        same(schema.validate('a')[0].path, undefined);

        end();
    });

    test('date-time', ({same, end}) => {
        const schema = createSchema({
            type: "string",
            format: 'date-time'
        });

        const expectedDescription = 'matches RFC3339 date-time profile';

        same(schema.description, `of type 'string' and ${expectedDescription}`);
        same(schema.validate('2016-04-06T10:10:09Z').length, 0);
        same(schema.validate('2016-04-06T10:10').length, 1);
        same(schema.validate('2016-04-06T10:10')[0].message, `must match RFC3339 date-time profile`);
        same(schema.validate('2016-04-06T10:10')[0].path, undefined);
        same(schema.validate('2016-04-06').length, 1);
        same(schema.validate('2016-04-06')[0].message, `must match RFC3339 date-time profile`);
        same(schema.validate('2016-04-06')[0].path, undefined);
        same(schema.validate('a').length, 1);
        same(schema.validate('a')[0].message, `must match RFC3339 date-time profile`);
        same(schema.validate('a')[0].path, undefined);
        end();
    });
});
