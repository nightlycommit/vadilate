import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('pattern', ({same, end}) => {
    const schema = createSchema({
        type: "string",
        pattern: '\\{a\\}\\d+'
    });

    const expectedDescription = 'match /\\{a\\}\\d+/';

    same(schema.description, `of type 'string' and ${expectedDescription}`);
    same(schema.validate('{a}5').length, 0);
    same(schema.validate('b').length, 1);
    same(schema.validate('b')[0].message, `must ${expectedDescription}`);
    same(schema.validate('b')[0].path, undefined);

    end();
});
