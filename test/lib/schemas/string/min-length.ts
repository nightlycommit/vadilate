import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('createMinLengthSchema', ({test}) => {
    test('maxLength positive', ({same, end}) => {
        const schema = createSchema({
            type: "string",
            minLength: 1
        });

        same(schema.description, 'of type \'string\' and longer than or equal to 1');
        same(schema.validate('1').length, 0);
        same(schema.validate('').length, 1);
        same(schema.validate('')[0].message, 'must be longer than or equal to 1');
        same(schema.validate('')[0].path, undefined);
        end();
    });
});
