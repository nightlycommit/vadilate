import * as tape from "tape";
import {createNumberSchema} from "../../../src/lib/schemas/number";

tape('createNumberSchema', ({test}) => {
    test('type', ({same, end}) => {
        const schema = createNumberSchema({
            type: "integer"
        });

        same(schema.validate(1).length, 0);
        same(schema.validate(1.1).length, 1);
        same(schema.validate(1.1)[0].rawMessage, 'must be of type \'integer\'');
        same(schema.validate(1.1)[0].path, undefined);

        end();
    });
});
