import * as tape from "tape";
import {createArraySchema} from "../../../src/lib/schemas/array";

tape('createArraySchema', ({test}) => {
    test('type', ({same, end}) => {
        const schema = createArraySchema({
            type: "array"
        });

        same(schema.validate([]).length, 0);
        same(schema.validate('1').length, 1);
        same(schema.validate('1')[0].rawMessage, 'must be of type \'array\'');
        same(schema.validate('1')[0].path, undefined);

        end();
    });

    test('items', ({same, end}) => {
        const schema = createArraySchema({
            type: "array",
            items: {
                oneOf: [{
                    type: "integer"
                }]
            },
        });

        same(schema.description, 'of type \'array\' and contains items of type \'integer\''
        );
        same(schema.validate([1, 2]).length, 0);

        const errors = schema.validate(['1', 1, 2, 1.1]);

        const expectedMessage: string = 'must be of type \'integer\'';

        same(errors.length, 2);
        same(errors[0].rawMessage, expectedMessage);
        same(errors[0].path, '0');
        same(errors[1].rawMessage, expectedMessage);
        same(errors[1].path, '3');
        end();
    });
});
