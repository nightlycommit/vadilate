import * as tape from "tape";
import {createStringSchema} from "../../../src/lib/schemas/string";

tape('createStringSchema', ({test}) => {
    test('type', ({same, end}) => {
        const schema = createStringSchema({
            type: "string"
        });

        same(schema.description, 'of type \'string\'');

        end();
    });
});