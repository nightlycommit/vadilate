import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('createTypeSchema', ({test}) => {
    test('type', ({test}) => {
        test('<undefined>', ({same, end}) => {
            const schema = createSchema({});

            same(schema.description, '');
            same(schema.validate(0).length, 0);
            same(schema.validate(null).length, 1);
            same(schema.validate(null)[0].message, 'must be not null');
            same(schema.validate(null)[0].path, undefined);

            test('nullable', ({test}) => {
                test('<undefined> or false', ({same, end}) => {
                    for (let nullable of [undefined, false]) {
                        const schema = createSchema({
                            nullable
                        });

                        same(schema.description, '');
                        same(schema.validate(0).length, 0);
                        same(schema.validate(null).length, 1);
                        same(schema.validate(null)[0].message, 'must be not null');
                        same(schema.validate(null)[0].path, undefined);
                    }

                    end();
                });

                test('true', ({same, end}) => {
                    const schema = createSchema({
                        nullable: true
                    });

                    same(schema.description, 'nullable');
                    same(schema.validate(0).length, 0);
                    same(schema.validate(null).length, 0);

                    end();
                });
            });

            end();
        });

        test('array', ({same, end}) => {
            const schema = createSchema({
                type: "array"
            });

            same(schema.description, 'of type \'array\'');
            same(schema.validate([1]).length, 0);
            same(schema.validate(0).length, 1);
            same(schema.validate(0)[0].message, 'must be of type \'array\'');
            same(schema.validate(0)[0].path, undefined);

            end();
        });

        test('boolean', ({same, end}) => {
            const schema = createSchema({
                type: "boolean"
            });

            same(schema.description, 'of type \'boolean\'');
            same(schema.validate(true).length, 0);
            same(schema.validate(false).length, 0);
            same(schema.validate(0).length, 1);
            same(schema.validate(0)[0].message, 'must be of type \'boolean\'');
            same(schema.validate(0)[0].path, undefined);
            end();
        });

        test('integer', ({same, end}) => {
            const schema = createSchema({
                type: "integer"
            });

            same(schema.description, 'of type \'integer\'');
            same(schema.validate(5).length, 0);
            same(schema.validate(5.5).length, 1);
            same(schema.validate(5.5)[0].message, 'must be of type \'integer\'');
            same(schema.validate(5.5)[0].path, undefined);
            end();
        });

        test('number', ({same, end}) => {
            const schema = createSchema({
                type: "number"
            });

            same(schema.description, 'of type \'number\'');
            same(schema.validate(5).length, 0);
            same(schema.validate(5.5).length, 0);
            same(schema.validate('0').length, 1);
            same(schema.validate('0')[0].message, 'must be of type \'number\'');
            same(schema.validate('0')[0].path, undefined);
            end();
        });

        test('object', ({same, end}) => {
            const schema = createSchema({
                type: "object"
            });

            same(schema.description, 'of type \'object\'');
            same(schema.validate({}).length, 0);
            same(schema.validate(0).length, 1);
            same(schema.validate(0)[0].message, 'must be of type \'object\'');
            same(schema.validate(0)[0].path, undefined);
            end();
        });

        test('string', ({same, end}) => {
            const schema = createSchema({
                type: "string"
            });

            same(schema.description, 'of type \'string\'');
            same(schema.validate('').length, 0);
            same(schema.validate(0).length, 1);
            same(schema.validate(0)[0].message, 'must be of type \'string\'');
            same(schema.validate(0)[0].path, undefined);
            end();
        });
    });

    test('nullable', ({test}) => {
        test('+ type', ({test}) => {
            test('<undefined> or false', ({same, end}) => {
                for (let nullable of [undefined, false]) {
                    const schema = createSchema({
                        type: 'array',
                        nullable
                    });

                    same(schema.description,  'of type \'array\'');
                    same(schema.validate(0).length, 1);
                    same(schema.validate(null).length, 1);
                    same(schema.validate(null)[0].message,  'must be of type \'array\'');
                    same(schema.validate(null)[0].path, undefined);
                    same(schema.validate(null).length, 1);
                    same(schema.validate(null)[0].message,  'must be of type \'array\'');
                    same(schema.validate(null)[0].path, undefined);
                }

                end();
            });

            test('true', ({same, end}) => {
                const schema = createSchema({
                    type: 'array',
                    nullable: true
                });

                same(schema.description, 'of type \'array\' and nullable');
                same(schema.validate(null).length, 0);
                same(schema.validate(0).length, 1);
                same(schema.validate(0)[0].message, 'must be of type \'array\' or null');
                same(schema.validate(0)[0].path, undefined);

                end();
            });
        });
    });
});
