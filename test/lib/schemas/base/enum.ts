import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('enum', ({test}) => {
    test('description', ({same, end}) => {
        same(createSchema({
            enum: [1]
        }).description, '1');

        same(createSchema({
            enum: [1, 2]
        }).description, 'either 1 or 2');

        same(createSchema({
            enum: [1, 2, 3]
        }).description, 'either 1 or 2 or 3');

        end();
    });

    test('validate', ({test}) => {
        test('with one item', ({same, end}) => {
            const schema = createSchema({
                enum: [1]
            });

            same(schema.validate(1).length, 0);

            const errors = schema.validate(2);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must be 1');
            same(errors[0].path, undefined);

            end();
        });

        test('with multiple items', ({same, end}) => {
            const schema = createSchema({
                enum: [1, 2]
            });

            same(schema.validate(1).length, 0);

            const errors = schema.validate(false);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must be either 1 or 2');
            same(errors[0].path, undefined);

            end();
        });

        test('with array', ({same, end}) => {
            const schema = createSchema({
                enum: [[1, 2, 3]]
            });

            same(schema.validate([1, 2, 3]).length, 0);

            same(schema.validate([1, 2]).length, 1);
            same(schema.validate([1, 2])[0].rawMessage, 'must be [ 1, 2, 3 ]');
            same(schema.validate([1, 2])[0].path, undefined);
            same(schema.validate(1).length, 1);
            same(schema.validate(1)[0].rawMessage, 'must be [ 1, 2, 3 ]');
            same(schema.validate(1)[0].path, undefined);

            end();
        });
    });
});