import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('allOf', ({test}) => {
    test('single sub-schema', ({same, end}) => {
        const schema = createSchema({
            allOf: [{
                type: "string"
            }]
        });

        const expectedDescription = '(of type \'string\')';

        same(schema.validate('5').length, 0);
        same(schema.description, expectedDescription);

        const errors = schema.validate(5);

        same(errors.length, 1);
        same(errors[0].rawMessage, `must be ${expectedDescription}`);
        same(errors[0].path, undefined);

        end();
    });

    test('multiple sub-schemas', ({same, end}) => {
        const schema = createSchema({
            allOf: [{
                type: "string",
                pattern: '^a'
            }, {
                type: "string",
                pattern: 'b$'
            }]
        });

        const expectedDescription = '(of type \'string\' and match /^a/) and (of type \'string\' and match /b$/)';

        same(schema.validate('ab').length, 0);
        same(schema.description, expectedDescription);

        const errors = schema.validate('a');

        same(errors.length, 1);
        same(errors[0].rawMessage, `must be ${expectedDescription}`);
        same(errors[0].path, undefined);

        end();
    });
});