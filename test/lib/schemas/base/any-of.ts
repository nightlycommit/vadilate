import * as tape from "tape";
import {createSchema} from "../../../../src";

/**
 * To validate against anyOf, the given data must be valid against any (one or more) of the given subschemas.
 *
 * @see https://json-schema.org/understanding-json-schema/reference/combining.html#anyof
 */
tape('anyOf', ({test, comment}) => {
    test('single sub-schema', ({same, end}) => {
        const schema = createSchema({
            anyOf: [{
                type: "string"
            }]
        });

        const expectedDescription = 'of type \'string\'';

        same(schema.validate('5').length, 0);
        same(schema.description, expectedDescription);

        const errors = schema.validate(5);

        same(errors.length, 1);
        same(errors[0].rawMessage, `must be ${expectedDescription}`);
        same(errors[0].path, undefined);

        end();
    });

    test('multiple sub-schemas', ({same, end}) => {
        const schema = createSchema({
            anyOf: [{
                type: "string",
                pattern: '^a'
            }, {
                type: "string",
                pattern: 'b$'
            }]
        });

        const expectedDescription = 'one or more of type \'string\' and match /^a/ and of type \'string\' and match /b$/';

        same(schema.validate('ab').length, 0);
        same(schema.validate('a').length, 0);
        same(schema.validate('b').length, 0);
        same(schema.description, expectedDescription);

        const errors = schema.validate('bc');

        same(errors.length, 1);
        same(errors[0].rawMessage, `must be ${expectedDescription}`);
        same(errors[0].path, undefined);

        end();
    });
});