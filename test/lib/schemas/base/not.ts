import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('not', ({same, end, test}) => {
    const schema = createSchema({
        not: {
            enum: [1, 2, 3]
        }
    });

    const expectedDescription = 'not either 1 or 2 or 3';

    same(schema.description, expectedDescription);
    same(schema.validate(4).length, 0);

    for (let value of [1, 2, 3]) {
        same(schema.validate(value).length, 1);
        same(schema.validate(value)[0].message, `must be ${expectedDescription}`);
        same(schema.validate(value)[0].path, undefined);
    }

    test('with nullable', ({same, end}) => {
        const schema = createSchema({
            not: {
                nullable: true
            }
        });

        same(schema.description, 'not nullable');

        end();
    });

    end();
});
