import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('oneOf', ({test}) => {
    test('single item', ({same, end}) => {
        const schema = createSchema({
            oneOf: [{
                type: "string"
            }]
        });

        const expectedDescription = 'of type \'string\'';

        same(schema.validate('5').length, 0);
        same(schema.description, expectedDescription);

        const errors = schema.validate(5);

        same(errors.length, 1);
        same(errors[0].rawMessage, `must be ${expectedDescription}`);
        same(errors[0].path, undefined);

        end();
    });

    test('multiple items', ({same, end}) => {
        const schema = createSchema({
            oneOf: [{
                type: "string"
            }, {
                type: "string",
                pattern: 'b'
            }]
        });

        const expectedDescription = 'either (of type \'string\') or (of type \'string\' and match /b/)';

        same(schema.validate('5').length, 0);
        same(schema.description, expectedDescription);

        const errors = schema.validate(5);

        same(errors.length, 1);
        same(errors[0].rawMessage, `must be ${expectedDescription}`);
        same(errors[0].path, undefined);

        end();
    });
});