import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('properties', ({test}) => {
    test('properties', ({same, end}) => {
        const schema = createSchema<{
            foo: string,
            bar: number
        }>({
            type: "object",
            properties: {
                foo: {
                    type: "string"
                }
            }
        });

        same(schema.description, 'of type \'object\' and with property foo (of type \'string\')');
        same(schema.validate({foo: '1'}).length, 0);
        same(schema.validate({foo: 1}).length, 1);
        same(schema.validate({foo: 1})[0].rawMessage, 'must be of type \'string\'');
        same(schema.validate({foo: 1})[0].path, 'foo');

        end();
    });

    test('additionalProperties', ({test}) => {
        test('false', ({same, end}) => {
            const schema = createSchema<{
                foo: string,
                bar: number
            }>({
                type: "object",
                properties: {
                    foo: {
                        type: "string"
                    }
                },
                additionalProperties: false
            });

            same(schema.description, 'of type \'object\' and with property foo (of type \'string\') and no additional properties');
            same(schema.validate({foo: '1'}).length, 0);
            same(schema.validate({foo: 1}).length, 1);
            same(schema.validate({foo: 1})[0].rawMessage, 'must be of type \'string\'');
            same(schema.validate({foo: 1})[0].path, 'foo');
            same(schema.validate({foo: '1'}).length, 0);
            same(schema.validate({foo: '1', bar: 2}).length, 1);
            same(schema.validate({foo: '1', bar: 2})[0].rawMessage, 'must not be present');
            same(schema.validate({foo: '1', bar: 2})[0].path, 'bar');

            end();
        });

        test('<undefined> or true', ({same, end}) => {
            for (let additionalProperties of [undefined, true]) {

                const schema = createSchema<{
                    foo: string,
                    bar: number
                }>({
                    type: "object",
                    properties: {
                        foo: {
                            type: "string"
                        }
                    },
                    additionalProperties
                });

                same(schema.description, 'of type \'object\' and with property foo (of type \'string\')');
                same(schema.validate({foo: '1'}).length, 0);
                same(schema.validate({foo: '1', bar: 2}).length, 0);
            }

            end();
        });

        test('schema', ({same, end}) => {
            const schema = createSchema<{
                foo: string,
                bar: boolean
            }>({
                type: "object",
                properties: {
                    foo: {
                        type: "string"
                    }
                },
                additionalProperties: {
                    type: "boolean"
                }
            });

            same(schema.description, 'of type \'object\' and with property foo (of type \'string\') and additional properties of type \'boolean\'');
            same(schema.validate({foo: '1'}).length, 0);
            same(schema.validate({foo: '1', bar: 2}).length, 1);
            same(schema.validate({foo: '1', bar: 2})[0].rawMessage, 'must be of type \'boolean\'');
            same(schema.validate({foo: '1', bar: 2})[0].path, 'bar');

            end();
        });
    });

    test('required', ({same, end}) => {
        const schema = createSchema<{
            foo: string,
            bar: number
        }>({
            type: "object",
            properties: {
                foo: {
                    type: "string"
                },
                bar: {
                    type: "string"
                }
            },
            required: [
                'foo'
            ]
        });

        same(schema.description, 'of type \'object\' and with property foo (of type \'string\') and required and with property bar (of type \'string\')');
        same(schema.validate({foo: '1'}).length, 0);
        same(schema.validate({}).length, 1);
        same(schema.validate({})[0].rawMessage, 'must be present');
        same(schema.validate({})[0].path, 'foo');

        end();
    });

    test('maxProperties', ({test}) => {
        test('errors', ({same, end}) => {
            const schema = createSchema({
                type: "object",
                maxProperties: 1
            });

            same(schema.validate({foo: '1'}).length, 0);
            same(schema.validate({foo: '1', bar: '1'}).length, 1);
            same(schema.validate({foo: '1', bar: '1'})[0].rawMessage, 'must contains at most 1 property');
            same(schema.validate({foo: '1', bar: '1'})[0].path, undefined);

            end();
        });

        test('description', ({test}) => {
            test('singular', ({same, end}) => {
                const schema = createSchema({
                    type: "object",
                    maxProperties: 1
                });

                same(schema.description, 'of type \'object\' and contains at most 1 property');

                end();
            });

            test('plural', ({same, end}) => {
                const schema = createSchema({
                    type: "object",
                    maxProperties: 2
                });

                same(schema.description, 'of type \'object\' and contains at most 2 properties');

                end();
            });
        });
    });

    test('minProperties', ({test}) => {
        test('errors', ({same, end}) => {
            const schema = createSchema({
                type: "object",
                minProperties: 1
            });

            same(schema.description, 'of type \'object\' and contains at least 1 property');
            same(schema.validate({foo: '1'}).length, 0);
            same(schema.validate({}).length, 1);
            same(schema.validate({})[0].rawMessage, 'must contains at least 1 property');
            same(schema.validate({})[0].path, undefined);

            end();
        });

        test('description', ({test}) => {
            test('singular', ({same, end}) => {
                const schema = createSchema({
                    type: "object",
                    minProperties: 1
                });

                same(schema.description, 'of type \'object\' and contains at least 1 property');

                end();
            });

            test('plural', ({same, end}) => {
                const schema = createSchema({
                    type: "object",
                    minProperties: 2
                });

                same(schema.description, 'of type \'object\' and contains at least 2 properties');

                end();
            });
        });
    });
});
