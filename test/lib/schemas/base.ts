import * as tape from "tape";
import {createBaseSchema} from "../../../src/lib/schemas/base";

tape('createBaseSchema', ({test}) => {
    test('description and title', ({same, end}) => {
        const schema = createBaseSchema({
            description: 'Foo description',
            title: 'Foo'
        });

        same(schema.description, '');

        end();
    });
});
