import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('multipleOf', ({test}) => {
    test('description', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            multipleOf: 10
        });

        same(schema.description, 'of type \'number\' and a multiple of 10');

        end();
    });

    test('with integers', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            multipleOf: 10
        });

        same(schema.validate(20).length, 0);
        same(schema.validate(11).length, 1);
        same(schema.validate(11)[0].message, 'must be a multiple of 10');
        same(schema.validate(11)[0].path, undefined);

        end();
    });

    test('with a float multipleOf', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            multipleOf: 10.1
        });

        same(schema.validate(202).length, 0);
        same(schema.validate(111).length, 1);
        same(schema.validate(111)[0].message, 'must be a multiple of 10.1');
        same(schema.validate(111)[0].path, undefined);

        end();
    });

    test('with a float value', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            multipleOf: 10.1
        });

        same(schema.validate(20.2).length, 0);
        same(schema.validate(111).length, 1);
        same(schema.validate(111)[0].message, 'must be a multiple of 10.1');
        same(schema.validate(111)[0].path, undefined);

        end();
    });
});
