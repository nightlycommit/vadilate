import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('minimum', ({test}) => {
    test('description', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            minimum: 1
        });

        same(schema.description, 'of type \'number\' and greater than or equal to 1');

        end();
    });

    test('exclusiveMinimum not set', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            minimum: 1
        });

        same(schema.validate(1).length, 0);
        same(schema.validate(0).length, 1);
        same(schema.validate(0)[0].message, 'must be greater than or equal to 1');
        same(schema.validate(0)[0].path, undefined);

        end();
    });

    test('exclusiveMinimum set to false', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            minimum: 1,
            exclusiveMinimum: false
        });

        same(schema.validate(1).length, 0);
        same(schema.validate(0).length, 1);
        same(schema.validate(0)[0].message, 'must be greater than or equal to 1');
        same(schema.validate(0)[0].path, undefined);

        end();
    });

    test('exclusiveMinimum set to true', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            minimum: 1,
            exclusiveMinimum: true
        });

        same(schema.validate(2).length, 0);
        same(schema.validate(1).length, 1);
        same(schema.validate(1)[0].message, 'must be greater than 1');
        same(schema.validate(0)[0].path, undefined);

        end();
    });
});
