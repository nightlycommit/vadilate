import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('maximum', ({test}) => {
    test('description', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            maximum: 1
        });

        same(schema.description, 'of type \'number\' and lower than or equal to 1');

        end();
    });

    test('exclusiveMaximum not set', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            maximum: 1
        });

        same(schema.validate(1).length, 0);
        same(schema.validate(2).length, 1);
        same(schema.validate(2)[0].message, 'must be lower than or equal to 1');
        same(schema.validate(2)[0].path, undefined);

        end();
    });

    test('exclusiveMaximum set to false', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            maximum: 1,
            exclusiveMaximum: false
        });

        same(schema.validate(1).length, 0);
        same(schema.validate(2).length, 1);
        same(schema.validate(2)[0].message, 'must be lower than or equal to 1');
        same(schema.validate(2)[0].path, undefined);

        end();
    });

    test('exclusiveMaximum set to true', ({same, end}) => {
        const schema = createSchema({
            type: "number",
            maximum: 1,
            exclusiveMaximum: true
        });

        same(schema.validate(0).length, 0);
        same(schema.validate(1).length, 1);
        same(schema.validate(1)[0].message, 'must be lower than 1');
        same(schema.validate(1)[0].path, undefined);

        end();
    });
});
