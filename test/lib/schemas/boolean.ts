import * as tape from "tape";
import {createBooleanSchema} from "../../../src/lib/schemas/boolean";

tape('createBooleanSchema', ({test}) => {
    test('type', ({same, end}) => {
        const schema = createBooleanSchema({
            type: "boolean"
        });

        same(schema.validate(true).length, 0);

        same(schema.validate('5').length, 1);
        same(schema.validate('5')[0].rawMessage, 'must be of type \'boolean\'');
        same(schema.validate('5')[0].path, undefined);

        end();
    });
});
