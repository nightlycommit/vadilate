import * as tape from "tape";
import {createObjectSchema} from "../../../src/lib/schemas/object";

tape('createObjectSchema', ({test}) => {
    test('path', ({same, end}) => {
        const schema = createObjectSchema({
            type: "object",
            properties: {
                foo: {
                    type: "integer"
                }
            }
        });

        same(schema.description, 'of type \'object\' and with property foo (of type \'integer\')');
        same(schema.validate({foo: 5}).length, 0);
        same(schema.validate({foo: '5'}).length, 1);
        same(schema.validate({foo: '5'})[0].rawMessage, 'must be of type \'integer\'');
        same(schema.validate({foo: '5'})[0].path, 'foo');

        end();
    });

    test('nested', ({test}) => {
        test('properties only', ({same, end}) => {
            const schema = createObjectSchema({
                type: "object",
                properties: {
                    foo: {
                        type: "object",
                        properties: {
                            bar: {
                                type: "boolean"
                            }
                        }
                    }
                }
            });

            same(schema.description, 'of type \'object\' and with property foo (of type \'object\' and with property bar (of type \'boolean\'))');
            same(schema.validate({foo: {bar: true}}).length, 0);
            same(schema.validate({foo: {bar: 'true'}}).length, 1);
            same(schema.validate({foo: {bar: 'true'}})[0].rawMessage, 'must be of type \'boolean\'');
            same(schema.validate({foo: {bar: 'true'}})[0].path, 'foo.bar');

            end();
        });

        test('properties + required', ({same, end}) => {
            const schema = createObjectSchema({
                type: "object",
                properties: {
                    foo: {
                        type: "object",
                        properties: {
                            bar: {
                                type: "boolean"
                            },
                            foo: {
                                type: "string"
                            }
                        },
                        required: [
                            'foo'
                        ]
                    }
                }
            });

            same(schema.description, 'of type \'object\' and with property foo (of type \'object\' and with property bar (of type \'boolean\') and with property foo (of type \'string\') and required)');
            same(schema.validate({foo: {bar: true, foo: '1'}}).length, 0);
            same(schema.validate({foo: {bar: true}}).length, 1);
            same(schema.validate({foo: {bar: true}})[0].rawMessage, 'must be present');
            same(schema.validate({foo: {bar: true}})[0].path, 'foo.foo');

            end();
        })
    });
});
