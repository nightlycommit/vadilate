import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('items', ({test}) => {
    test('description', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            items: {
                oneOf: [{
                    type: "string"
                }, {
                    type: "number"
                }]
            }
        });

        same(schema.description, 'of type \'array\' and contains items either (of type \'string\') or (of type \'number\')');

        end();
    });

    test('oneOf', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            items: {
                oneOf: [{
                    type: "string"
                }, {
                    type: "number"
                }]
            }
        });

        same(schema.validate(['1']).length, 0);
        same(schema.validate([1]).length, 0);

        end();
    })
});
