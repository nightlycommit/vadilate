import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('uniqueItems', ({test}) => {
    test('description', ({test}) => {
        test('<undefined> or false', ({same, end}) => {
            for (let uniqueItems of [undefined, false]) {
                const schema = createSchema({
                    type: "array",
                    uniqueItems
                });

                same(schema.description, 'of type \'array\'');
            }

            end();
        });

        test('true', ({same, end}) => {
            const schema = createSchema({
                type: "array",
                uniqueItems: true
            });

            same(schema.description, 'of type \'array\' and contains only unique items');

            end();
        });
    });

    test('validate', ({test}) => {
        test('true', ({same, end}) => {
            const schema = createSchema({
                type: "array",
                uniqueItems: true
            });

            same(schema.validate([1, 2]).length, 0);
            same(schema.validate([]).length, 0);
            same(schema.validate([1, 1]).length, 1);
            same(schema.validate([1, 1])[0].message, 'must contain only unique items');
            same(schema.validate([1, 1])[0].path, undefined);

            end();
        });

        test('false', ({same, end}) => {
            const schema = createSchema({
                type: "array",
                uniqueItems: false
            });

            same(schema.validate([1, 1]).length, 0);
            same(schema.validate([]).length, 0);
            same(schema.validate([1, 2]).length, 0);

            end();
        });
    });
});
