import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('minItems', ({test}) => {
    test('description', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            minItems: 1
        });

        same(schema.description, 'of type \'array\' and contain at least 1 element');

        end();
    });

    test('minLength lower than 2', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            minItems: 1
        });

        same(schema.validate([1]).length, 0);
        same(schema.validate([]).length, 1);
        same(schema.validate([])[0].message, 'must contain at least 1 element');
        same(schema.validate([])[0].path, undefined);
        end();
    });

    test('minLength greater than 1', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            minItems: 2
        });

        same(schema.validate([1, 1]).length, 0);
        same(schema.validate([1]).length, 1);
        same(schema.validate([1])[0].message, 'must contain at least 2 elements');
        same(schema.validate([1])[0].path, undefined);
        end();
    });
});
