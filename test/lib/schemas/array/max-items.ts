import * as tape from "tape";
import {createSchema} from "../../../../src";

tape('maxItems', ({test}) => {
    test('description', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            maxItems: 1
        });

        same(schema.description, 'of type \'array\' and contain at most 1 element');

        end();
    });

    test('maxLength lower than 2', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            maxItems: 1
        });

        same(schema.validate([1]).length, 0);
        same(schema.validate([1, 1]).length, 1);
        same(schema.validate([1, 1])[0].message, 'must contain at most 1 element');
        same(schema.validate([1, 1])[0].path, undefined);
        end();
    });

    test('maxLength greater than 1', ({same, end}) => {
        const schema = createSchema({
            type: "array",
            maxItems: 2
        });

        same(schema.validate([1, 1]).length, 0);
        same(schema.validate([1, 1, 1]).length, 1);
        same(schema.validate([1, 1, 1])[0].message, 'must contain at most 2 elements');
        same(schema.validate([1, 1, 1])[0].path, undefined);
        end();
    });
});
