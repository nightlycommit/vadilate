import * as tape from "tape";
import {createSchema} from "../../src";

tape('createSchema', ({same, end}) => {
    same(createSchema({
        type: "array",
        minItems: 1,
        maxItems: 2,
        uniqueItems: true
    }).validate([1, 2]).length, 0);

    same(createSchema({
        type: "number",
        minimum: 2,
        maximum: 3,
        multipleOf: 2
    }).validate(2).length, 0);

    same(createSchema({
        type: "object",
        minProperties: 1,
        maxProperties: 2
    }).validate({
        foo: 'bar'
    }).length, 0);

    same(createSchema({
        type: "string",
        minLength: 1,
        maxLength: 3
    }).validate('12').length, 0);

    same(createSchema({
        allOf: [{
            type: "integer"
        }]
    }).validate(5).length, 0);

    same(createSchema({
        anyOf: [{
            type: "integer"
        }]
    }).validate(5).length, 0);

    same(createSchema({
        not: {
            type: "integer"
        }
    }).validate('5').length, 0);

    end();
});