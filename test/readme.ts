import * as tape from "tape";
import {createSchema} from "../src";

tape('readme', ({same, end}) => {
    const schema = createSchema({
        type: "string",
        enum: ['Validate']
    });

    const errors = schema.validate("Vadilate");

    same(errors.length, 1);
    same(errors[0].message, "must be 'Validate'");

    end();
});