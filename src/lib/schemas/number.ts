import {Schema, TypedSchemaDefinition} from "../schema";
import {createSchemaDescription, inspect} from "../util";
import {ValidationError} from "../error";
import {createBaseSchema} from "./base";

type MaximumSchemaDefinition = {
    maximum: number;
    exclusiveMaximum?: boolean;
}

const createMaximumSchema = (definition: MaximumSchemaDefinition): Schema<number> => {
    const {maximum, exclusiveMaximum} = definition;

    const description: string = exclusiveMaximum === true ?
        `lower than ${inspect(maximum)}` : `lower than or equal to ${inspect(maximum)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            const isValid = (exclusiveMaximum === true) ? (value < maximum) : (value <= maximum);

            if (!isValid) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type MinimumSchemaDefinition = {
    minimum: number;
    exclusiveMinimum?: boolean;
};

const createMinimumSchema = (definition: MinimumSchemaDefinition): Schema<number> => {
    const {minimum, exclusiveMinimum} = definition;

    const description: string = exclusiveMinimum === true ?
        `greater than ${inspect(minimum)}` : `greater than or equal to ${inspect(minimum)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            const isValid = (exclusiveMinimum === true) ? (value > minimum) : (value >= minimum);

            if (!isValid) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type MultipleOfSchemaDefinition = {
    multipleOf: number;
};

const createMultipleOfSchema = (definition: MultipleOfSchemaDefinition): Schema<number> => {
    const {multipleOf} = definition;

    const description: string = `a multiple of ${inspect(multipleOf)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            const valueParts = String(value).split('.');
            const multipleOfParts = String(multipleOf).split('.');

            const valueNumberOfDecimals = valueParts.length > 1 ? valueParts[1].length : 0;
            const multipleOfNumberOfDecimals = multipleOfParts.length > 1 ? multipleOfParts[1].length : 0;

            const rate = Math.pow(10, Math.max(valueNumberOfDecimals, multipleOfNumberOfDecimals));

            if (((value * rate) % (multipleOf * rate)) !== 0) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

export type NumberSchemaDefinition = TypedSchemaDefinition<"integer" | "number"> &
    Partial<MultipleOfSchemaDefinition> &
    Partial<MinimumSchemaDefinition> &
    Partial<MaximumSchemaDefinition> & {
    format?: string
};

export const createNumberSchema = (definition: NumberSchemaDefinition): Schema<number> => {
    const {maximum, minimum, exclusiveMaximum, exclusiveMinimum, multipleOf} = definition;

    const subSchemas: Array<Schema<any>> = [
        createBaseSchema(definition)
    ];

    if (minimum !== undefined) {
        subSchemas.push(createMinimumSchema({minimum, exclusiveMinimum}));
    }

    if (maximum !== undefined) {
        subSchemas.push(createMaximumSchema({maximum, exclusiveMaximum}));
    }

    if (multipleOf !== undefined) {
        subSchemas.push(createMultipleOfSchema({multipleOf}));
    }

    return {
        description: createSchemaDescription(subSchemas),
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(value));
            }

            return errors;
        }
    };
}