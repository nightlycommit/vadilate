import {createSchema, Schema, SchemaDefinition} from "../schema";
import {ValidationError} from "../error";
import {compare, createSchemaDescription, inspect} from "../util";

type AllOfSchemaDefinition = {
    allOf: [SchemaDefinition, ...SchemaDefinition[]];
};

/**
 * allOf: each item must be valid against all the schemas
 */
const createAllOfSchema = (definition: AllOfSchemaDefinition): Schema<Array<any>> => {
    const {allOf} = definition;

    const subSchemas = allOf.map((definition) => createSchema(definition));

    const description = `(${createSchemaDescription(subSchemas, ') and (')})`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            const schemasErrors: Array<ValidationError> = [];

            for (let schema of subSchemas) {
                schemasErrors.push(...schema.validate(value));
            }

            if (schemasErrors.length > 0) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type AnyOfSchemaDefinition = {
    anyOf: Array<SchemaDefinition>;
}

const createAnyOfSchema = (definition: AnyOfSchemaDefinition): Schema<any> => {
    const {anyOf} = definition;

    const subSchemas = anyOf.map((definition) => createSchema(definition as any));
    const subSchemasDescription = createSchemaDescription(subSchemas);

    const description = subSchemas.length > 1 ? `one or more ${subSchemasDescription}` : subSchemasDescription;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            let isValid: boolean = false;

            for (let schema of subSchemas) {
                const schemaErrors = schema.validate(value);

                if (schemaErrors.length === 0) {
                    isValid = true;
                    break;
                }
            }

            if (!isValid) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type EnumSchemaDefinition<T> = {
    enum: [T, ...T[]]
};

const createEnumSchema = <T>(definition: EnumSchemaDefinition<T>): Schema<T> => {
    const {enum: choices} = definition;

    const description = choices.length > 1 ?
        `either ${choices.map((choice) => inspect(choice)).join(' or ')}` :
        `${inspect(choices[0])}`
    ;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            let isValid: boolean = false;

            for (let choice of choices) {
                if (compare(value, choice)) {
                    isValid = true;

                    break;
                }
            }

            if (!isValid) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    }
}

type NotSchemaDefinition = {
    not: SchemaDefinition;
}

const createNotSchema = (definition: NotSchemaDefinition): Schema<any> => {
    const {not} = definition;

    const schema = createSchema(not as any);

    const description = `not ${schema.description}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (schema.validate(value).length === 0) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    }
};

type OneOfSchemaDefinition = {
    oneOf: [SchemaDefinition, ...SchemaDefinition[]]
};

/**
 * oneOf
 *
 * Validates the value against exactly one of the subschemas
 */
const createOneOfSchema = (definition: OneOfSchemaDefinition): Schema<Array<any>> => {
    const {oneOf} = definition;

    const schemas = oneOf.map((definition) => createSchema(definition as any));

    const description = schemas.length > 1 ?
        `either (${createSchemaDescription(schemas, ') or (')})` :
        `${createSchemaDescription(schemas)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];
            const validSchemas: Array<Schema<any>> = [];

            for (let schema of schemas) {
                const schemaErrors = schema.validate(value);

                if (schemaErrors.length === 0) {
                    validSchemas.push(schema);
                }
            }

            if (validSchemas.length === 0) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type TypeSchemaDefinition = {
    type?: string;
    nullable?: boolean;
};

const createTypeSchema = (definition: TypeSchemaDefinition): Schema<any> => {
    const {type, nullable} = definition;

    const descriptionParts: Array<string> = [];
    const messageParts: Array<string> = [];

    if (type !== undefined) {
        const typeDescription = `of type ${inspect(type)}`;

        descriptionParts.push(typeDescription);
        messageParts.push(typeDescription);

        if (nullable === true) {
            descriptionParts.push(' and nullable');
            messageParts.push(' or null');
        }
    }
    else if (nullable === true) {
        descriptionParts.push('nullable');
    }
    else {
        messageParts.push('not null');
    }

    const description = descriptionParts.join('');
    const message = messageParts.join('');

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (type !== undefined) {
                if ((value !== null) || (nullable !== true)) {
                    const isValid = (type === 'array') ? Array.isArray(value) :
                        (type === 'integer') ? Number.isInteger(value) :
                            typeof value === type;

                    if (!isValid) {
                        errors.push(new ValidationError(`must be ${message}`));
                    }
                }
            } else if ((value === null) && (nullable !== true)) {
                errors.push(new ValidationError(`must be ${message}`));
            }

            return errors;
        }
    };
}

export type BaseSchemaDefinition =
    {
        type?: undefined;
        nullable?: boolean;
        description?: string;
        title?: string;
    } &
    Partial<OneOfSchemaDefinition> &
    Partial<AllOfSchemaDefinition> &
    Partial<AnyOfSchemaDefinition> &
    Partial<NotSchemaDefinition> &
    Partial<EnumSchemaDefinition<any>>;

export const createBaseSchema = (definition: SchemaDefinition): Schema<any> => {
    const {
        allOf,
        anyOf,
        enum: choices,
        not,
        nullable,
        oneOf,
        type
    } = definition;

    const subSchemas: Array<Schema<any>> = [];

    subSchemas.push(createTypeSchema({type, nullable}));

    if (allOf !== undefined) {
        subSchemas.push(createAllOfSchema({allOf}));
    }

    if (anyOf !== undefined) {
        subSchemas.push(createAnyOfSchema({anyOf}));
    }

    if (choices !== undefined) {
        subSchemas.push(createEnumSchema({enum: choices}));
    }

    if (not !== undefined) {
        subSchemas.push(createNotSchema({not}));
    }

    if (oneOf !== undefined) {
        subSchemas.push(createOneOfSchema({oneOf}));
    }

    return {
        description: createSchemaDescription(subSchemas),
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(value));
            }

            return errors;
        }
    }
}