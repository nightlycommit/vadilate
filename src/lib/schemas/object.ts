import {createSchema, Schema, SchemaDefinition, TypedSchemaDefinition} from "../schema";
import {ValidationError} from "../error";
import {createSchemaDescription} from "../util";
import {createBaseSchema} from "./base";

type MinPropertiesSchemaDefinition = {
    minProperties: number;
};

type MaxPropertiesSchemaDefinition = {
    maxProperties: number
};

type PropertiesSchemaDefinition<T> = {
    properties?: {
        [P in keyof T]?: SchemaDefinition
    };
    required?: Array<keyof T>;
    additionalProperties?: boolean | SchemaDefinition;
} & Partial<MinPropertiesSchemaDefinition> &
    Partial<MaxPropertiesSchemaDefinition>;

const createMinPropertiesSchema = (definition: MinPropertiesSchemaDefinition): Schema<Array<any>> => {
    const {minProperties} = definition;

    const description = `contains at least ${minProperties} ${minProperties > 1 ? 'properties' : 'property'}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            const keys = Object.keys(value);

            if (keys.length < minProperties) {
                errors.push(new ValidationError(`must ${description}`));
            }

            return errors;
        }
    }
};

const createMaxPropertiesSchema = (definition: MaxPropertiesSchemaDefinition): Schema<Array<any>> => {
    const {maxProperties} = definition;

    const description = `contains at most ${maxProperties} ${maxProperties > 1 ? 'properties' : 'property'}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            const keys = Object.keys(value);

            if (keys.length > maxProperties) {
                errors.push(new ValidationError(`must ${description}`));
            }

            return errors;
        }
    }
};

const createPropertiesSchema = <T>(definition: PropertiesSchemaDefinition<T>): Schema<any> => {
    const {properties, additionalProperties, required} = definition;

    const subSchemas: Array<Schema<any>> = [];

    const createError = (propertyName: string, {rawMessage, path}: ValidationError): ValidationError => {
        const parts: Array<string> = [
            propertyName
        ];

        if (path) {
            parts.push(path);
        }

        return new ValidationError(rawMessage, parts.join('.'));
    }

    // properties and required
    for (let propertyName in properties) {
        const schema = createSchema(properties[propertyName] as SchemaDefinition);

        const descriptionParts = [
            `with property ${propertyName} (${schema.description})`
        ];

        if (required !== undefined) {
            if (required.includes(propertyName)) {
                descriptionParts.push('required');
            }
        }

        const propertySchema: Schema<any> = {
            description: descriptionParts.join(' and '),
            validate: (object) => {
                const errors: Array<ValidationError> = [];
                const value = object[propertyName];

                // check required presence
                if (value === undefined) {
                    if ((required !== undefined) && (required.includes(propertyName))) {
                        errors.push(new ValidationError(`must be present`, propertyName));
                    }
                } else {
                    errors.push(...schema.validate(value).map((error) => createError(propertyName, error)));
                }

                return errors;
            }
        };

        subSchemas.push(propertySchema);
    }

    // additionalProperties
    if ((properties !== undefined) && (additionalProperties !== undefined)) {
        if (typeof additionalProperties === 'boolean') {
            // By default any additional properties are allowed.
            if (additionalProperties === false) {
                subSchemas.push({
                    description: `no additional properties`,
                    validate: (object) => {
                        const errors: Array<ValidationError> = [];
                        const propertiesKeys = Object.keys(properties);

                        for (let objectKey of Object.keys(object)) {
                            if (!propertiesKeys.includes(objectKey)) {
                                errors.push(new ValidationError(`must not be present`, objectKey));
                            }
                        }

                        return errors;
                    }
                });
            }
        } else {
            const {description, validate} = createSchema(additionalProperties);

            subSchemas.push({
                description: `additional properties ${description}`,
                validate: (object) => {
                    const errors: Array<ValidationError> = [];
                    const propertiesKeys = Object.keys(properties);

                    for (let objectKey of Object.keys(object)) {
                        if (!propertiesKeys.includes(objectKey)) {
                            errors.push(...validate(object[objectKey]).map((error) => createError(objectKey, error)));
                        }
                    }

                    return errors;
                }
            });
        }
    }

    const description = createSchemaDescription(subSchemas);

    return {
        description,
        validate: (object) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(object));
            }

            return errors;
        }
    };
}

export type ObjectSchemaDefinition<T> = TypedSchemaDefinition<"object"> &
    Partial<PropertiesSchemaDefinition<T>>;

/**
 * Convenient method to create a Object schema.
 */
export const createObjectSchema = <T>(definition: ObjectSchemaDefinition<T>): Schema<T> => {
    const {
        properties,
        required, additionalProperties,
        minProperties,
        maxProperties
    } = definition;

    const subSchemas: Array<Schema<any>> = [
        createBaseSchema(definition)
    ];

    if (properties !== undefined) {
        subSchemas.push(createPropertiesSchema({properties, required, additionalProperties}));
    }

    if (minProperties !== undefined) {
        subSchemas.push(createMinPropertiesSchema({minProperties}));
    }

    if (maxProperties !== undefined) {
        subSchemas.push(createMaxPropertiesSchema({maxProperties}));
    }

    return {
        description: createSchemaDescription(subSchemas),
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(value));
            }

            return errors;
        }
    };
}