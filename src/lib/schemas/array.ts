import type {Schema, TypedSchemaDefinition} from "../schema";
import {createSchema, SchemaDefinition} from "../schema";
import {ValidationError} from "../error";
import {compare, createSchemaDescription} from "../util";
import {createBaseSchema} from "./base";

type ItemsSchemaDefinition = {
    items: SchemaDefinition
};

const createItemsSchema = (definition: ItemsSchemaDefinition): Schema<Array<any>> => {
    const {items} = definition;

    const schema = createSchema(items as any);
    const description = `contains items ${schema.description}`;

    return {
        description,
        validate: (values) => {
            const errors: Array<ValidationError> = [];

            let index: number = 0;

            for (let value of values) {
                const valueErrors = schema.validate(value);

                if (valueErrors.length > 0) {
                    errors.push(new ValidationError(`must be ${schema.description}`, `${index}`));
                }

                index++;
            }

            return errors;
        }
    }
}

type MaxItemsSchemaDefinition = {
    maxItems: number
};

const createMaxItemsSchema = (definition: MaxItemsSchemaDefinition): Schema<Array<any>> => {
    const {maxItems} = definition;

    const description = `contain at most ${maxItems} element${maxItems > 1 ? 's' : ''}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (value.length > maxItems) {
                errors.push(new ValidationError(`must ${description}`));
            }

            return errors;
        }
    }
};

type MinItemsSchemaDefinition = {
    minItems: number
};

const createMinItemsSchema = (definition: MinItemsSchemaDefinition): Schema<Array<any>> => {
    const {minItems} = definition;

    const description = `contain at least ${minItems} element${minItems > 1 ? 's' : ''}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (value.length < minItems) {
                errors.push(new ValidationError(`must ${description}`));
            }

            return errors;
        }
    }
};

type UniqueItemsSchemaDefinition = {
    uniqueItems: boolean
};

const createUniqueItemsSchema = (definition: UniqueItemsSchemaDefinition): Schema<any> => {
    const {uniqueItems} = definition;

    const description = uniqueItems === true ? 'contains only unique items' : '';

    return {
        description,
        validate: (values) => {
            const errors: Array<ValidationError> = [];

            if (uniqueItems === true) {
                const encounteredValues: Array<any> = [];

                let isValid: boolean = true;

                for (let value of values) {
                    if (encounteredValues.find((encounteredValue) => compare(value, encounteredValue))) {
                        isValid = false;
                        break;
                    } else {
                        encounteredValues.push(value);
                    }
                }

                if (!isValid) {
                    errors.push(new ValidationError(`must contain only unique items`));
                }
            }

            return errors;
        }
    }
}

export type ArraySchemaDefinition = TypedSchemaDefinition<"array"> &
    Partial<ItemsSchemaDefinition> &
    Partial<UniqueItemsSchemaDefinition> &
    Partial<MaxItemsSchemaDefinition> &
    Partial<MinItemsSchemaDefinition>;

/**
 * Convenient method to create a Array schema.
 */
export const createArraySchema = <T>(definition: ArraySchemaDefinition): Schema<Array<T>> => {
    const {items, uniqueItems, maxItems, minItems} = definition;

    const subSchemas: Array<Schema<any>> = [
        createBaseSchema(definition)
    ];

    if (items !== undefined) {
        subSchemas.push(createItemsSchema({items}));
    }

    if (minItems !== undefined) {
        subSchemas.push(createMinItemsSchema({minItems}));
    }

    if (maxItems !== undefined) {
        subSchemas.push(createMaxItemsSchema({maxItems}));
    }

    if (uniqueItems !== undefined) {
        subSchemas.push(createUniqueItemsSchema({uniqueItems}));
    }

    return {
        description: createSchemaDescription(subSchemas),
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(value));
            }

            return errors;
        }
    };
}