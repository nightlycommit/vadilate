import type {Schema, TypedSchemaDefinition} from "../schema";
import {createSchemaDescription, inspect} from "../util";
import {ValidationError} from "../error";
import {createBaseSchema} from "./base";

type MaxLengthSchemaDefinition = {
    maxLength: number;
}

const createMaxLengthSchema = (definition: MaxLengthSchemaDefinition): Schema<string | Array<any>> => {
    const {maxLength} = definition;

    const description: string = `shorter than or equal to ${inspect(maxLength)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (value.length > maxLength) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type MinLengthSchemaDefinition = {
    minLength: number;
}

const createMinLengthSchema = (definition: MinLengthSchemaDefinition): Schema<string | Array<any>> => {
    const {minLength} = definition;

    const description: string = `longer than or equal to ${inspect(minLength)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (value.length < minLength) {
                errors.push(new ValidationError(`must be ${description}`));
            }

            return errors;
        }
    };
};

type PatternSchemaDefinition = {
    pattern: string;
}

const createPatternSchema = (definition: PatternSchemaDefinition): Schema<string> => {
    const {pattern} = definition;

    const regularExpression = new RegExp(pattern);

    const description: string = `match ${inspect(regularExpression)}`;

    return {
        description,
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            if (!regularExpression.test(value)) {
                errors.push(new ValidationError(`must ${description}`));
            }

            return errors;
        }
    };
};

export type StringSchemaDefinition = TypedSchemaDefinition<"string"> &
    Partial<MaxLengthSchemaDefinition> &
    Partial<MinLengthSchemaDefinition> &
    Partial<PatternSchemaDefinition> & {
    format?: 'date' | 'date-time' | string
};

/**
 * Convenient method to create a String schema.
 */
export const createStringSchema = (definition: StringSchemaDefinition): Schema<string> => {
    const {maxLength, minLength, pattern, format} = definition;

    const subSchemas: Array<Schema<any>> = [
        createBaseSchema(definition)
    ];

    if (minLength !== undefined) {
        subSchemas.push(createMinLengthSchema({minLength}));
    }

    if (maxLength !== undefined) {
        subSchemas.push(createMaxLengthSchema({maxLength}));
    }

    if (pattern !== undefined) {
        subSchemas.push(createPatternSchema({pattern}));
    }

    // format
    if (format === "date") {
        subSchemas.push({
            description: 'matches RFC3339 full-date profile',
            validate: (value) => {
                const schema = createPatternSchema({pattern: `^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])$`});

                const errors: Array<ValidationError> = [];

                if (schema.validate(value).length > 0) {
                    errors.push(new ValidationError(`must match RFC3339 full-date profile`));
                }

                return errors;
            }
        });
    }

    if (format === "date-time") {
        subSchemas.push({
            description: 'matches RFC3339 date-time profile',
            validate: (value) => {
                const schema = createPatternSchema({pattern: `^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$`});

                const errors: Array<ValidationError> = [];

                if (schema.validate(value).length > 0) {
                    errors.push(new ValidationError(`must match RFC3339 date-time profile`));
                }

                return errors;
            }
        });
    }

    return {
        description: createSchemaDescription(subSchemas),
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(value));
            }

            return errors;
        }
    };
};