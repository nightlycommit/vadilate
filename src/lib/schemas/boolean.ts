import {Schema, TypedSchemaDefinition} from "../schema";
import {createBaseSchema} from "./base";

export type BooleanSchema = Schema<boolean>;

export type BooleanSchemaDefinition = TypedSchemaDefinition<"boolean"> & {
   format?: string
};

/**
 * Convenient method to create a boolean schema.
 */
export const createBooleanSchema = (definition: BooleanSchemaDefinition): BooleanSchema => {
   return createBaseSchema(definition);
}