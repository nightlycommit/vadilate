import {ValidationError} from "./error";
import {createSchemaDescription} from "./util";
import {createStringSchema, StringSchemaDefinition} from "./schemas/string";
import {createNumberSchema, NumberSchemaDefinition} from "./schemas/number";
import {createArraySchema, ArraySchemaDefinition} from "./schemas/array";
import {createBooleanSchema, BooleanSchemaDefinition} from "./schemas/boolean";
import {createObjectSchema, ObjectSchemaDefinition} from "./schemas/object";
import {BaseSchemaDefinition, createBaseSchema} from "./schemas/base";

export type TypedSchemaDefinition<T extends 'array' | 'boolean' | 'integer' | 'number' | 'object' | 'string'> =
    Omit<BaseSchemaDefinition, "type">
    & { type: T; };

export type Schema<T> = {
    description: string,
    validate: (value: any) => Array<ValidationError>
};

export type SchemaDefinition<T = any> = BaseSchemaDefinition
    | ArraySchemaDefinition
    | BooleanSchemaDefinition
    | NumberSchemaDefinition
    | ObjectSchemaDefinition<T>
    | StringSchemaDefinition;

export function createSchema<T>(definition: SchemaDefinition<T>): Schema<any> {
    const subSchemas: Array<Schema<any>> = [];

    // to benefit from TS type narrowing, we don't use const {type} = definition here
    if (definition.type === undefined) {
        subSchemas.push(createBaseSchema(definition));
    }

    if (definition.type === "array") {
        subSchemas.push(createArraySchema(definition));
    }

    if (definition.type === "boolean") {
        subSchemas.push(createBooleanSchema(definition));
    }

    if (definition.type === "integer" || definition.type === "number") {
        subSchemas.push(createNumberSchema(definition));
    }

    if (definition.type === "object") {
        subSchemas.push(createObjectSchema(definition));
    }

    if (definition.type === "string") {
        subSchemas.push(createStringSchema(definition));
    }

    return {
        description: createSchemaDescription(subSchemas),
        validate: (value) => {
            const errors: Array<ValidationError> = [];

            for (let subSchema of subSchemas) {
                errors.push(...subSchema.validate(value));
            }

            return errors;
        }
    }
}