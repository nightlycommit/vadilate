import {inspect as nativeInspect} from "util";
import {Schema} from "./schema";

export const inspect = (value: any) => nativeInspect(value, false, 1);

export const createSchemaDescription = (subSchemas: Array<Schema<any>>, separator: string = ' and '): string => {
    const subSchemasDescriptions = subSchemas.map((schema) => schema.description).filter((description) => description !== '');

    return `${subSchemasDescriptions.join(separator)}`;
}

export const compare = (value: any, to: any): boolean => {
    if (!Array.isArray(value) || !Array.isArray(to)) {
        return value == to;
    }

    if (value.length !== to.length) {
        return false;
    }

    let result: boolean = true;
    let index: number = 0;

    for (let valueItem of value) {
        const toItem: any = to[index];

        result &&= compare(valueItem, toItem);

        index++;
    }

    return result;
};
